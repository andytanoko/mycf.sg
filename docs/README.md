# Overview

This is an Answer for Technical Checkin. This repository contains a simple program written in Golang named pinger. This service reponds with "hello world" at the root path and can be configured to ping another server through the environment variables (see the file at ./cmd/pinger/config.go). By default, running it will make it start a server and ping itself. The output if this project is a downloadable artifact.

- - -

## Get Started

1. Clone this repository
2. To build locally, run command:  `docker build -f ./deployments/build/Dockerfile -t devops/pinger:latest . should result in a successful image named devops/pinger:latest`
3. To run locally as a single service, run command: `docker run -it -p 8000:8000 devops/pinger:latest` 
4. To run command: `docker-compose -f ./deployments/docker-compose.yml up`
5. There is an automatic  CI/CD process, triggered for every git push command. It will create a downloadable artifact in the CI/CD jobs page. Output details is described in the Output section
6. Artifact build is using versioning. The versioning is using the ymd.HM format

- - -

## Output

The output for this project is a downloadable artifact. It can be downloaded from job page. 
The artifact will be expire in 1 week. After the expiry date you will not be able to download the artifact.

| File | Description |
| --- | --- |
| build/pinger-${version}.jar | Docker image file in tar format |
| build/main-${version}.jar | binary file |

`${version}` represents version number in `ymd.HM` format 

Example:

| File | Description |
| --- | --- |
| build/pinger-200420.0526.jar | Docker image file in tar format |
| build/main-200420.0526.jar | binary file |


- - - 



## Directory structure

| Directory | Description |
| --- | --- |
| `/bin` | Contains binaries |
| `/cmd` | Contains source code for CLI interfaces |
| `/deployments` | Contains image files and manifests for deployments |
| `/docs` | Contains documentation |
| `/vendor` | Contains dependencies (use `make dep` to populate it) |



- - -



# License

Code is licensed under the [MIT license](./LICENSE).

Content is licensed under the [Creative Commons 4.0 (Attribution) license](https://creativecommons.org/licenses/by-nc-sa/4.0/).



